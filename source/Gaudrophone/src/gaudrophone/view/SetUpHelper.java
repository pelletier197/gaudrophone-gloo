/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.view;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

/**
 * Small container to help the set up of a main fonctionnality view that passes
 * by many set up views.
 *
 * @author sunny
 */
public final class SetUpHelper {

    private Queue<Screen> queuedScreens;

    private List<Object> setUpParameters;

    public SetUpHelper(Screen... screens) {
        queuedScreens = new ArrayDeque<>();
        setUpParameters = new ArrayList<>();
        pushNextScreens(screens);
    }

    public void pushNextScreens(Screen... screens) {
        queuedScreens.addAll(Arrays.asList(screens));
    }

    public Screen getNextScreen() {
        return queuedScreens.remove();
    }

    public void addParameter(Object obj) {
        setUpParameters.add(obj);
    }

    public Object getParameter(int index) {
        return index < setUpParameters.size() ? setUpParameters.get(index) : null;
    }

    public List<Object> getParameters() {
        return setUpParameters;
    }
}
