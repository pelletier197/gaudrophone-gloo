/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.view.views;

import gaudrophone.controller.GaudrophoneController;
import gaudrophone.view.ControlledScreen;
import gaudrophone.view.ScreenController;
import gaudrophone.view.SetUpHelper;
import javax.swing.JPanel;

/**
 *
 * @author sunny
 */
public abstract class ViewBase extends JPanel implements ControlledScreen {

    protected ScreenController controller;
    protected GaudrophoneController godClass;

    public ViewBase() {
        this.godClass = new GaudrophoneController();
    }

    @Override
    public void setScreenController(ScreenController controller) {
        this.controller = controller;
    }

    @Override
    public void removedFromScreen() {
        godClass = new GaudrophoneController();
    }

    @Override
    public void displayedToScreen(SetUpHelper helper) {
        // Deletes controller to save RAM and processing
        godClass.dissociate();
        godClass = null;
        System.gc();
    }

}
