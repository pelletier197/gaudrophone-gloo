/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller.DTO;

import gaudrophone.controller.GeneratedInstrumentType;
import gaudrophone.controller.InstrumentType;

/**
 *
 * @author sunny
 */
public class SoundDTO {

    private int note;
    private String noteName;
    private int octave;
    private double volume;
    private long persistance;
    private String filePath;
    private int timbre;
    private GeneratedInstrumentType generatedInstrumentType;
    private double frequency;

    public int getTimbre() {
        return timbre;
    }

    public void setTimbre(int timbre) {
        this.timbre = timbre;
    }

    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public int getOctave() {
        return octave;
    }

    public void setOctave(int octave) {
        this.octave = octave;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public long getPersistance() {
        return persistance;
    }

    public void setPersistance(long persistance) {
        this.persistance = persistance;
    }

    public GeneratedInstrumentType getGeneratedInstrumentType() {
        return generatedInstrumentType;
    }

    public void setGeneratedInstrumentType(GeneratedInstrumentType generatedInstrumentType) {
        this.generatedInstrumentType = generatedInstrumentType;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

}
