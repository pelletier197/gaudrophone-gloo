/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.controller.helpers;

import gaudrophone.controller.DTO.BorderDTO;
import gaudrophone.controller.DTO.InstrumentKeyDTO;
import gaudrophone.controller.DTO.ShapeDTO;
import gaudrophone.controller.DTO.SoundDTO;
import gaudrophone.controller.GeneratedInstrumentType;
import gaudrophone.controller.ShapeType;
import gaudrophone.drawing.SwingUtils;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.sound.Sound;
import gaudrophone.instrument.sound.file.FileSound;
import gaudrophone.instrument.sound.generated.GeneratedSound;
import gaudrophone.instrument.apparence.Border;
import gaudrophone.instrument.apparence.BorderType;
import gaudrophone.instrument.geometry.Ellipse;
import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;
import gaudrophone.instrument.geometry.Shape;
import gaudrophone.instrument.geometry.shapes.Diamond;
import gaudrophone.instrument.geometry.shapes.Hexagon;
import gaudrophone.instrument.geometry.shapes.Octogon;
import gaudrophone.instrument.geometry.shapes.Pentagon;
import gaudrophone.instrument.geometry.shapes.Rectangle;
import gaudrophone.instrument.geometry.shapes.Trapezoid;
import gaudrophone.instrument.geometry.shapes.Triangle;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.sound.generated.GeneratedSoundException;
import gaudrophone.instrument.sound.generated.GeneratedSoundInstrument;
import gaudrophone.instrument.sound.generated.GeneratedSoundManager;
import java.awt.Color;

/**
 *
 * @author olivi
 */
public class DTOHelper {
    
    public static InstrumentKeyDTO InstrumentKeyToDTO(InstrumentKey instrumentKey) {
        InstrumentKeyDTO dto = new InstrumentKeyDTO();
        dto.setColor(ColorToAWTColor(instrumentKey.getBackgroundColor()));
        dto.setName(instrumentKey.getName());
        dto.setShape(ShapeToDTO(instrumentKey.getShape()));
        dto.setSound(SoundToDTO(instrumentKey.getSound()));
        dto.setBorder(BorderToDTO(instrumentKey.getBorder()));
        dto.setImageFile(instrumentKey.getImageFile());
        dto.setDisplay(instrumentKey.toString());
        dto.setUuid(instrumentKey.getUuid());
        
        dto.setNameDisplayed(instrumentKey.isNameDisplayed());
        dto.setNoteAndOctaveDisplayed(instrumentKey.isNoteAndOctaveDisplayed());
        dto.setFrequencyDisplayed(instrumentKey.isFrequencyDisplayed());
        dto.setPersistanceDisplayed(instrumentKey.isPersistanceDisplayed());
        dto.setVolumeDisplayed(instrumentKey.isVolumeDisplayed());
        
        dto.setPlayable(instrumentKey.isPlayable());
        
        return dto;
    }
    
    public static InstrumentKey DTOToInstrumentKey(InstrumentKeyDTO dto) throws GeneratedSoundException {
        InstrumentKey instrumentKey = new InstrumentKey();
        instrumentKey.setShape(DTOToShape(dto.getShape()));
        instrumentKey.setName(dto.getName());
        instrumentKey.setSound(DTOToSound(dto.getSound()));
        instrumentKey.setBackgroundColor(AWTColorToColor(dto.getColor()));
        instrumentKey.setBorder(DTOToBorder(dto.getBorder()));
        
        return instrumentKey;
    }
    
    //TODO Support more shapes like elipsis etc  (now only support polygons)
    public static ShapeDTO ShapeToDTO(Shape shape) {
        ShapeDTO dto = new ShapeDTO();
        dto.setWidth(shape.getWidth());
        dto.setHeight(shape.getHeight());
        dto.setPosX(shape.getCenter().getX());
        dto.setPosY(shape.getCenter().getY());
        dto.setRotationAngle(shape.getAngle());
        
        if (shape instanceof Diamond) {
            dto.setShapeType(ShapeType.DIAMOND);
        } else if (shape instanceof Hexagon) {
            dto.setShapeType(ShapeType.HEXAGON);
        } else if (shape instanceof Octogon) {
            dto.setShapeType(ShapeType.OCTOGON);
        } else if (shape instanceof Pentagon) {
            dto.setShapeType(ShapeType.PENTAGON);
        } else if (shape instanceof Rectangle) {
            dto.setShapeType(ShapeType.RECTANGLE);
        } else if (shape instanceof Trapezoid) {
            dto.setShapeType(ShapeType.TRAPEZOID);
        } else if (shape instanceof Triangle) {
            dto.setShapeType(ShapeType.TRIANGLE);
        } else if (shape instanceof Ellipse) {
            dto.setShapeType(ShapeType.ELLIPSE);
        }
        
        return dto;
    }

    //TODO Support more shapes like elipsis etc  (now only support polygons)
    public static Shape DTOToShape(ShapeDTO dto) {
        Shape shape;
        ShapeType shapeType = dto.getShapeType();
        
        switch (shapeType) {
            case DIAMOND:
                shape = new Diamond();
                break;
            case HEXAGON:
                shape = new Hexagon();
                break;
            case OCTOGON:
                shape = new Octogon();
                break;
            case PENTAGON:
                shape = new Pentagon();
                break;
            case RECTANGLE:
                shape = new Rectangle();
                break;
            case TRAPEZOID:
                shape = new Trapezoid();
                break;
            case TRIANGLE:
                shape = new Triangle();
                break;
            default:
                throw new UnsupportedOperationException("ShapeType " + shapeType.toString() + " construction is not supported yet");
        }
        
        shape.setAngle(dto.getRotationAngle());
        shape.setHeight(dto.getHeight());
        shape.setWidth(dto.getWidth());
        shape.setPosition(new Point(dto.getPosX(), dto.getPosY()));
        
        return shape;
    }
    
    public static SoundDTO SoundToDTO(Sound sound) {
        SoundDTO dto = new SoundDTO();
        dto.setNote(sound.getNote().getNoteNumber());
        dto.setNoteName(sound.getNote().toString());
        dto.setOctave(sound.getOctave());
        dto.setPersistance(sound.getPersistence());
        dto.setVolume(sound.getVolume());
        dto.setFrequency(sound.getFrequency());
        
        if (sound instanceof FileSound) {
            dto.setFilePath(((FileSound) sound).getFilePath());
        } else {
            dto.setGeneratedInstrumentType(GeneratedInstrumentType.values()[((GeneratedSound) sound).getInstrument().ordinal()]);
        }
        
        return dto;
    }
    
    public static Sound DTOToSound(SoundDTO dto) throws GeneratedSoundException {
        Sound sound;
        
        if (dto.getFilePath() != null && !dto.getFilePath().isEmpty()) {
            FileSound fileSound = new FileSound();
            fileSound.setFilePath(dto.getFilePath());
            
            sound = fileSound;
        } else {
            GeneratedSound generatedSound = new GeneratedSound(Note.getNoteFromOrdinal(dto.getNote()), dto.getOctave(), GeneratedSoundInstrument.getInstrumentFromOrdinal(dto.getTimbre()));
            sound = generatedSound;
        }
        
        sound.setNote(Note.getNoteFromOrdinal(dto.getNote()));
        sound.setOctave(dto.getOctave());
        sound.setPersistence(dto.getPersistance());
        sound.setVolume(dto.getVolume());
        
        return sound;
    }
    
    public static BorderDTO BorderToDTO(Border border) {
        
        BorderDTO dto = new BorderDTO();
        dto.setWidth(border.getWidth());
        dto.setColor(ColorToAWTColor(border.getColor()));
        dto.setBorderType(border.getType().toString());
        
        return dto;
    }
    
    public static Border DTOToBorder(BorderDTO dto) {
        Border border = new Border();
        border.setColor(AWTColorToColor(dto.getColor()));
        border.setWidth(dto.getWidth());
        border.setType(BorderType.fromString(dto.getBorderType()));
        
        return border;
    }
    
    private static Color ColorToAWTColor(gaudrophone.instrument.apparence.Color color) {
        return SwingUtils.fromInstrumentColor(color);
    }
    
    private static gaudrophone.instrument.apparence.Color AWTColorToColor(Color color) {
        return SwingUtils.toInstrumentColor(color);
    }
}
