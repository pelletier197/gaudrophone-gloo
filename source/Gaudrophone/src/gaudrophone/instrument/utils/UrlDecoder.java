/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.utils;

import gaudrophone.instrument.IO.GaudrophoneIOManager;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sunny
 */
public class UrlDecoder {

    public static final String getDecodedPath(String basePath) {
        try {
            return URLDecoder.decode(basePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static final String getResourcePath(String resourceName) {
        return getDecodedPath(UrlDecoder.class.getClass().getResource(resourceName).getFile());
    }
}
