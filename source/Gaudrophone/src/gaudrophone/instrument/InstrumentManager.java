/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument;

import gaudrophone.instrument.IO.GaudrophoneIOManager;
import gaudrophone.instrument.template.GuitarTemplateStrategy;
import gaudrophone.instrument.template.InstrumentTemplateStrategy;
import gaudrophone.instrument.template.PianoTemplateStrategy;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 * @author sunny
 */
public class InstrumentManager {

    /**
     * The unique instance of this class
     */
    private static InstrumentManager instance;

    public static InstrumentManager getInstance() {
        if (instance == null) {
            instance = new InstrumentManager();
        }

        return instance;
    }

    private final GaudrophoneIOManager ioManager;

    private InstrumentManager() {
        this.ioManager = new GaudrophoneIOManager();
    }

    public Instrument loadCustomInstrumentFromName(String name) {
        return ioManager.loadInstrumentFromName(name);
    }

    public Set<String> getCustomInstrumentsNames() {
        return ioManager.getCustomInstrumentNames();
    }

    public Instrument getInstrumentFromStrategyName(String strategyName) {
        InstrumentTemplateStrategy strat = getTemplateStrategies().stream().filter(item -> item.toString().equals(strategyName)).findFirst().orElse(null);
        return strat == null ? null : strat.generateInstrument();
    }

    public List<InstrumentTemplateStrategy> getTemplateStrategies() {
        return Arrays.asList(new InstrumentTemplateStrategy[]{
            new GuitarTemplateStrategy(),
            new PianoTemplateStrategy()
        });
    }

    public List<String> getTemplateStrategyNames() {
        return getTemplateStrategies().stream().map(m -> m.toString()).collect(Collectors.toList());
    }

    public boolean deleteInstrumentFromName(String name){
        return ioManager.deleteInstrumentFromName(name);
    }
    
    public void saveCustomInstrument(Instrument instrument) {
        ioManager.saveInstrument(instrument);
    }
}
