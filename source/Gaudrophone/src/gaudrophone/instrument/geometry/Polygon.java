/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry;

import gaudrophone.instrument.apparence.Border;
import java.io.Serializable;
import java.util.Arrays;

/**
 *
 * @author sunny
 */
public class Polygon extends AbstractShape implements Serializable {

    private Point[] points;

    private double[] rectangleBounds;
    private boolean isRectangleComputed;

    /**
     * This constructor makes a basic rectangle polygon.
     */
    public Polygon() {
        this(new Point(-1, -1),
                new Point(-1, 1),
                new Point(1, 1),
                new Point(1, -1));
    }

    public Polygon(Point... points) {
        if (points == null) {
            throw new NullPointerException("Null points");
        }
        if (points.length < 2) {
            throw new IllegalArgumentException("Not enough points");
        }

        this.points = points;
        computeCenter();
        isRectangleComputed = false;
    }

    /**
     * Tests either if the point is contained inside the polygon. This function
     * uses ray casting method, and the algorithm is taken and adapted from
     * rosettacode.org
     *
     * @see https://rosettacode.org/wiki/Ray-casting_algorithm
     * @param p The point to test
     * @return True if the point is inside the polygon, false otherwise
     */
    @Override
    public boolean isInside(Point p) {

        checkBounds();

        if (!isInsideRectangleBounds(p)) {
            return false;
        }

        double x = p.x, y = p.y;
        int hits = 0;

        Point lastPoint = points[points.length - 1];
        double lastx = lastPoint.x;
        double lasty = lastPoint.y;
        double curx, cury;

        Point currentPoint;

        // Walk the edges of the polygon
        for (int i = 0; i < points.length; lastx = curx, lasty = cury, i++) {
            currentPoint = points[i];
            curx = currentPoint.x;
            cury = currentPoint.y;

            if (cury == lasty) {
                continue;
            }

            double leftx;
            if (curx < lastx) {
                if (x >= lastx) {
                    continue;
                }
                leftx = curx;
            } else {
                if (x >= curx) {
                    continue;
                }
                leftx = lastx;
            }

            double test1, test2;
            if (cury < lasty) {
                if (y < cury || y >= lasty) {
                    continue;
                }
                if (x < leftx) {
                    hits++;
                    continue;
                }
                test1 = x - curx;
                test2 = y - cury;
            } else {
                if (y < lasty || y >= cury) {
                    continue;
                }
                if (x < leftx) {
                    hits++;
                    continue;
                }
                test1 = x - lastx;
                test2 = y - lasty;
            }

            if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
                hits++;
            }
        }

        // If odd
        return ((hits & 1) != 0);
    }

    @Override
    public void rotate(double deltaDegrees) {
        super.rotate(deltaDegrees); // Updates angle

        double radians = Math.toRadians(deltaDegrees);

        double cosAngle = Math.cos(radians);
        double sinAngle = Math.sin(radians);

        for (Point p : points) {
            //TRANSLATE TO ORIGIN
            double x1 = p.getX() - center.x;
            double y1 = p.getY() - center.y;

            //APPLY ROTATION
            double tempX1 = (x1 * cosAngle) - (y1 * sinAngle);
            double tempY1 = (x1 * sinAngle) + (y1 * cosAngle);

            //TRANSLATE BACK
            p.x = tempX1 + center.x;
            p.y = tempY1 + center.y;
        }
        isRectangleComputed = false;
    }

    @Override
    public void translate(double deltaX, double deltaY) {
        super.translate(deltaX, deltaY); // Translate center

        for (Point p : points) {
            p.x += deltaX;
            p.y += deltaY;
        }

        isRectangleComputed = false;
    }

    @Override
    public void scale(double scaleX, double scaleY) {

        if (scaleX == 0 || scaleY == 0) {
            throw new IllegalArgumentException("Illegal scale factor");
        }

        for (Point point : points) {
            point.x = scaleX * (point.x - center.x) + center.x;
            point.y = scaleY * (point.y - center.y) + center.y;
        }

        // The bounds must be computed again
        isRectangleComputed = false;
    }

    /**
     *
     * @return
     */
    @Override
    public double getHeight() {
        checkBounds();
        // MaxY - MinY
        return rectangleBounds[3] - rectangleBounds[2];
    }

    @Override
    public double getWidth() {
        checkBounds();
        // MaxX - MinX
        return rectangleBounds[1] - rectangleBounds[0];
    }

    @Override
    public Point[] getPoints() {
        return Arrays.copyOf(points, points.length);
    }

    @Override
    public String toString() {
        return "Shape{" + "points=" + Arrays.toString(points) + '}';
    }

    private void checkBounds() {
        if (!isRectangleComputed) {
            computeRectangleBounds();
        }
    }

    private void computeRectangleBounds() {
        double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
        double maxX = -Double.MAX_VALUE, maxY = -Double.MAX_VALUE;

        double x, y;

        for (Point point : points) {
            x = point.x;
            y = point.y;
            minX = Math.min(minX, x);
            maxX = Math.max(maxX, x);
            minY = Math.min(minY, y);
            maxY = Math.max(maxY, y);
        }

        rectangleBounds = new double[]{minX, maxX, minY, maxY};
        isRectangleComputed = true;
    }

    private void computeCenter() {

        final int size = points.length;
        double sumX = 0, sumY = 0;

        for (Point p : points) {
            sumX += p.x;
            sumY += p.y;
        }

        this.center = new Point(sumX / size, sumY / size);
    }

    private boolean isInsideRectangleBounds(Point p) {
        return p.x >= rectangleBounds[0] && p.x <= rectangleBounds[1] && p.y >= rectangleBounds[2] && p.y <= rectangleBounds[3];
    }

    @Override
    public void translate(Point pDeltaPosition) {
        translate(pDeltaPosition.x, pDeltaPosition.y);
    }

}
