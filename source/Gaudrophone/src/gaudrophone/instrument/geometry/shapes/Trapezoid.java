/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.geometry.shapes;

import gaudrophone.instrument.geometry.Point;
import gaudrophone.instrument.geometry.Polygon;

/**
 *
 * @author sunny
 */
public class Trapezoid extends Polygon {

    public Trapezoid() {
        super(new Point[]{
            new Point(0, 1),
            new Point(0.25, 0),
            new Point(0.75, 0),
            new Point(1,1)
        });
    }

}
