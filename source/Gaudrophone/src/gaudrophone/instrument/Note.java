/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument;

/**
 *
 * @author sunny
 */
public enum Note {

    C(0, "Do", "C"),
    C_SHARP(1, "Do#", "C#"),
    D(2, "Ré", "D"),
    D_SHARP(3, "Ré#", "D#"),
    E(4, "Mi", "E"),
    F(5, "Fa", "F"),
    F_SHARP(6, "Fa#", "F#"),
    G(7, "Sol", "G"),
    G_SHARP(8, "Sol#", "G#"),
    A(9, "La", "A"),
    A_SHARP(10, "La#", "A#"),
    B(11, "Si", "B");

    private final String display;
    private final String englishName;
    private final int noteNumber;

    private Note(int noteNumber, String french, String english) {
        this.noteNumber = noteNumber;
        this.englishName = english;
        this.display = String.format("%s (%s)", french, english);
    }

    public int getNoteNumber() {
        return noteNumber;
    }

    public static Note getNoteFromOrdinal(int i) {
        return Note.values()[i];
    }
    
    public static Note getNoteFromEnglishName(String englishName){
        for (Note b : Note.values()) {
            if (b.englishName.equals(englishName)) {
                return b;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return display;
    }

    public static Note fromString(String toString) {
        for (Note b : Note.values()) {
            if (b.toString().equalsIgnoreCase(toString)) {
                return b;
            }
        }
        return null;
    }

}
