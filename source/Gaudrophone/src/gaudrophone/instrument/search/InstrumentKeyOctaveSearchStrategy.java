/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.search;

import gaudrophone.instrument.InstrumentKey;

/**
 *
 * @author sunny
 */
public class InstrumentKeyOctaveSearchStrategy extends AbstractInstrumentKeySearchStrategy {

    @Override
    public boolean filter(InstrumentKey key) {
        String octaveToString = Integer.toString(key.getSound().getOctave());
        return octaveToString.equals(this.expression);
    }


}
