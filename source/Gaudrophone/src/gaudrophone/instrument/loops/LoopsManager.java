/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.loops;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.music.MusicalEventListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olivi
 */
public class LoopsManager {
    
    private final List<Loop> loops;
    
    public LoopsManager(Instrument instr, MusicalEventListener eventListener) {
        loops = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            loops.add(new Loop(instr, eventListener));
        }
    }
    
    public LoopState loopKeyPressed(int loopIndex) {
        Loop loop = loops.get(loopIndex);
        switch (loop.getState()) {
            case STOPPED:
                loop.startRecording();
                break;
            case RECORDING:
                loop.stopRecording();
                loop.startPlaying();
                break;
            case PLAYING:
                loop.stopPlaying();
                loop.clear();
                break;
        }
        return loop.getState();
    }
    
    public void instrumentKeyPressed(InstrumentKey key) {
        loops.stream().filter((loop) -> (loop.getState().equals(LoopState.RECORDING))).forEachOrdered((loop) -> {
            loop.recordKeyPress(key);
        });
    }
    
    public void instrumentKeyReleased(InstrumentKey key) {
        loops.stream().filter((loop) -> (loop.getState().equals(LoopState.RECORDING))).forEachOrdered((loop) -> {
            loop.recordKeyRelease(key);
        });
    }
    
    public void stopAll() {
        loops.forEach(m -> m.stopPlaying());
    }
}
