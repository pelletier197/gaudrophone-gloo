/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound;

import gaudrophone.instrument.Note;
import java.io.Serializable;

/**
 *
 * @author sunny
 */
public abstract class Sound implements Serializable {

    //-frequence : int
    private Note note;
    //-octave : int
    private int octave;
    //-persistence : double
    private long persistence;
    //-volume : double
    private double volume;
    private double frequency;
    private long startTime;
    private Thread stopThread = null;

    public Note getNote() {
        return note;
    }

    public final void setNote(Note pNote) {
        stopSound(); // Force the old sound to stop, or it won't be possible to stop it after
        if (pNote == null) {
            throw new NullPointerException();
        }

        this.note = pNote;
        updateFrequency();
    }

    public int getOctave() {
        return octave;
    }

    public final void setOctave(int pOctave) {
        stopSound();
        if (pOctave < 0 || pOctave > 8) {
            throw new IllegalArgumentException("Octave must be between 0 and 8.");
        }

        this.octave = pOctave;
        updateFrequency();
    }

    public long getPersistence() {
        return persistence;
    }

    public void setPersistence(long pPersistence) {
        stopSound();
        if (pPersistence < 0) {
            throw new IllegalArgumentException("Persistance cannot be less than 0.");
        }

        this.persistence = pPersistence;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double pVolume) {
        stopSound();
        if (pVolume < 0 || pVolume > 1) {
            throw new IllegalArgumentException("Volume must be between 0 and 1.");
        }

        this.volume = pVolume;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequencyNew) {
        if (this.frequency != frequencyNew) {
            stopSound();

            int midiNote = computeMidiNoteFromFrequency(frequencyNew);
            setMidiNote(midiNote);
            this.frequency = frequencyNew;
        }
    }

    private void updateFrequency() {
        this.frequency = computeFrequencyFromNoteOctave(getNote(), getOctave());
    }

    public int getMidiNote() {
        return computeMidiNoteFromNoteOctave(getNote(), getOctave());
    }

    public void setMidiNote(int midiNote) {
        stopSound();
        setNote(Note.getNoteFromOrdinal(midiNote % 12));
        setOctave(midiNote / 12 - 1);
    }

    public Sound() {
        this(Note.C, 4);
    }

    public Sound(Note pNote, int pOctave) {
        setNote(pNote);
        setOctave(pOctave);
        persistence = 50;
        volume = 1;
    }

    public void play() {
        startTime = System.currentTimeMillis();
        if (stopThread != null) {
            stopThread.interrupt();
            stopThread = null;
            stopSound();
        }
        playSound();
    }

    public void stop() {
        if (stopThread != null) {
            return; // sound is already being stopped
        }
        long playedLength = System.currentTimeMillis() - startTime;
        // sound has to stop
        if (persistence <= playedLength) {
            stopSound();
            return;
        }
        long timeToSleep = persistence - playedLength;
        stopThread = new Thread(() -> {
            try {
                Thread.sleep(timeToSleep);
                stopSound();
            } catch (InterruptedException ex) {
            }
            stopThread = null;
        });
        stopThread.start();
    }

    //+produireSon() : void
    public abstract void playSound();

    //+cesserSon() : void
    public abstract void stopSound();

    public static int computeMidiNoteFromNoteOctave(Note note, int octave) {
        return octave * 12 + note.getNoteNumber() + 12;
    }

    public static double computeFrequencyFromNoteOctave(Note note, int octave) {
        int midiNote = computeMidiNoteFromNoteOctave(note, octave) - 12;
        return Math.pow(2, (midiNote - 57.0) / 12) * 440;
    }

    public static int computeMidiNoteFromFrequency(double frequency) {
        return (int) (12 * Math.log(frequency / 440) / Math.log(2) + 57);
    }
}
