/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.sound.generated;

import gaudrophone.instrument.Note;
import gaudrophone.instrument.sound.Sound;

/**
 *
 * @author sunny
 */
public class GeneratedSound extends Sound {

    private final GeneratedSoundInstrument instrument;

    private boolean playing;

    public GeneratedSoundInstrument getInstrument() {
        return instrument;
    }

    /**
     * This constructor makes a default sound.
     */
    public GeneratedSound() {
        super();
        this.instrument = GeneratedSoundInstrument.PIANO;
    }

    public GeneratedSound(Note note, int octave, GeneratedSoundInstrument instrument) {
        super(note, octave);
        this.instrument = instrument;
    }

    @Override
    public void playSound() {
        if (!playing) {
            GeneratedSoundManager.getInstance().playNote(getMidiNote(), instrument.getMidiInstrument(), getVolume(), instrument.getPreferredChannel());
            playing = true;
        }
    }

    @Override
    public void stopSound() {
        if (playing) {
            GeneratedSoundManager.getInstance().stopNote(getMidiNote(), instrument.getMidiInstrument());
            playing = false;
        }
    }
}
