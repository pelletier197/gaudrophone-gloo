/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.gaudrohero;

import gaudrophone.instrument.InstrumentKey;
import gaudrophone.instrument.music.Composition;
import gaudrophone.instrument.music.MusicBox;
import gaudrophone.instrument.music.MusicalEventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

/**
 *
 * @author sunny
 */
public class GaudroHero {

    /**
     * The maximum delay for key press before and after the event occurs
     */
    private static final long MAXIMUM_DELAY_BEFORE = 500;

    private static final long MAX_TIMEOUT_FOR_SIMULTANEOUS_NOTES = 10;

    private static final int MIN_SCORE_MULTIPLIER = 1;
    private static final int MAX_SCORE_MULTIPLIER = 5;
    private static final int NUMBER_OF_STREAK_FOR_MULTIPLIER_UP = 5;
    private static final int POINTS_PER_SUCCES = 1;

    private static final int POINT_HISTORY_MAX_SIZE = 10;

    /**
     * The current composition being played
     */
    private MusicBox musicBox;

    /**
     * The streak count is calculated from the number of streak notes the player
     * hits. If a user hits a n
     */
    private int currentStreakCount;

    private int points;
    private int scoreMultiplier;
    private LinkedList<ScoreEvent> scoreHistory;

    /**
     * The key is the same as the value, but the key's comparator is used to
     * acces real data's value using a temporary object as key for get method.
     */
    private LinkedList<InstrumentKeySequence> keyEvents;
    private LinkedList<InstrumentKeyTimeStamp> usersKeyPressed;

    /**
     * Used to remove past events from the map to avoid memory filling
     */
    private Timer keyRemovingTimer;

    private long lastMusicalEventTime;
    private InstrumentKeySequence lastSequence;
    private MusicalEventListener musicalEventListener;

    private long pauseBeginTime;

    private int successNoteCount;
    private int failedNoteCount;
    private int totalSequenceCount;

    public GaudroHero() {
        this.keyEvents = new LinkedList<>();
        this.usersKeyPressed = new LinkedList<>();
        this.scoreHistory = new LinkedList<>();
        this.keyRemovingTimer = new Timer();

    }

    public void play() {
        if (musicBox == null) {
            throw new IllegalStateException("No composition is set");
        }

        musicBox.setDemo(true);
        musicBox.resume();

        if (pauseBeginTime != 0) {
            long pauseDuration = System.currentTimeMillis() - pauseBeginTime;

            // Updates the events so the user wont miss
            keyEvents.forEach(m -> m.timestamp += pauseDuration);
            usersKeyPressed.forEach(m -> m.timestamp += pauseDuration);

        } else {
            points = 0;
            scoreHistory.clear();
            successNoteCount = 0;
            failedNoteCount = 0;
            totalSequenceCount = 0;
            missStreak();
        }

        pauseBeginTime = 0;
    }

    public void pause() {
        if (musicBox == null) {
            throw new IllegalStateException("No composition is set");
        }
        pauseBeginTime = System.currentTimeMillis();
        musicBox.stop();
        keyRemovingTimer.cancel();
        keyRemovingTimer = new Timer();
    }

    public void restart() {
        if (musicBox == null) {
            throw new IllegalStateException("No composition is set");
        }
        this.scoreMultiplier = MIN_SCORE_MULTIPLIER;
        this.points = 0;
        pauseBeginTime = 0;
        this.musicBox.start();
    }

    public void setMusicBox(MusicBox musicBox) {
        if (musicBox == null) {
            throw new NullPointerException("Cannot set a null composition");
        }

        if (this.musicBox != null && this.musicBox.isPlaying()) {
            throw new IllegalStateException("Can't set the composition while playing");
        }

        this.musicBox = musicBox;
        musicBox.setMusicEventListener(getEventListener());

        this.scoreMultiplier = MIN_SCORE_MULTIPLIER;
        this.points = 0;
    }

    public void pressKey(InstrumentKey key) {
        if (musicBox == null || !musicBox.isPlaying()) {
            throw new IllegalStateException("No playing currently");
        }
        handlePlayerPressKey(key);
    }

    public int getPoints() {
        return points;
    }

    public int getScoreMultiplier() {
        return scoreMultiplier;
    }

    public LinkedList<ScoreEvent> getScoreHistory() {
        return scoreHistory;
    }

    public boolean isPlaying() {
        return musicBox != null && musicBox.isPlaying();
    }

    public void setSpeed(double fraction) {
        this.musicBox.setSpeed(fraction);
    }

    public double getGaudroScore() {

        if (totalSequenceCount == 0 || failedNoteCount == 0) {
            return 1.0;
        }
        return ((double) successNoteCount / (totalSequenceCount + failedNoteCount));
    }

    public int getSuccessCount() {
        return this.successNoteCount;
    }

    public int getFailedCount() {
        return this.failedNoteCount;
    }

    private void handleMusicEvent(InstrumentKey key) {

        long currentTime = System.currentTimeMillis();

        // The user pressed the key before the event occurs
        if (getInstrumentKeyTimeStamp(key) != null) {

            InstrumentKeySequence sequence = getInstrumentKeySequenceAndCreateIfNotExist(key);
            sequence.keys.forEach(m -> {
                if (m != key) {
                    m.press();
                }
            });
            if (!sequence.succededPlaying) {
                sequence.succededPlaying = true;
                handleSuccessPress(key);
            }
            lastSequence = sequence;
        } else {

            if (currentTime - lastMusicalEventTime < MAX_TIMEOUT_FOR_SIMULTANEOUS_NOTES) {
                lastSequence.keys.add(key);
            } else {

                InstrumentKeySequence sequence = createSequence();
                lastSequence = sequence;
                sequence.keys.add(key);
            }
        }
        lastMusicalEventTime = currentTime;
    }

    private InstrumentKeySequence getInstrumentKeySequenceAndCreateIfNotExist(InstrumentKey key) {
        InstrumentKeySequence sequence = getInstrumentKeySequence(key);

        // The sequence was not created yet
        if (sequence == null) {
            sequence = createSequence();
            sequence.keys.add(key);
        }

        return sequence;
    }

    private InstrumentKeySequence createSequence() {
        InstrumentKeySequence sequence = new InstrumentKeySequence(System.currentTimeMillis());
        lastSequence = sequence;
        keyEvents.add(sequence);
        totalSequenceCount++;
        return sequence;
    }

    /**
     * Returns and removes the first instrument key timestamp that matches the
     * given key in
     *
     * @param the key to look for
     * @return The first key that matches
     */
    private InstrumentKeyTimeStamp getInstrumentKeyTimeStamp(InstrumentKey key) {

        Iterator<InstrumentKeyTimeStamp> itr = usersKeyPressed.listIterator();
        InstrumentKeyTimeStamp current;

        while (itr.hasNext()) {
            current = itr.next();

            if (key == current.key) {
                itr.remove();
                return current;
            }
        }

        return null;
    }

    /**
     * Returns and removes the first instrument key timestamp that matches the
     * given key in
     *
     * @param the key to look for
     * @return The first key that matches
     */
    private InstrumentKeySequence getInstrumentKeySequence(InstrumentKey key) {

        Iterator<InstrumentKeySequence> itr = keyEvents.listIterator();
        InstrumentKeySequence current;

        while (itr.hasNext()) {
            current = itr.next();

            if (current.keys.contains(key)) {
                return current;
            }
        }

        return null;
    }

    private void handlePlayerPressKey(InstrumentKey key) {

        // Finds the first sequence to match the key
        InstrumentKeySequence test = getInstrumentKeySequence(key);

        long currentTime = System.currentTimeMillis();

        // The user pressed the key after the event occurs
        if (test != null) {

            if (!test.succededPlaying) {
                test.succededPlaying = true;
                handleSuccessPress(key);
                test.keys.forEach(k -> k.press()); // Pressed all keys of sequence
            }

        } else {
            // The maybe pressed the key before the musical event occurs, so it is stored
            InstrumentKeyTimeStamp stamp = new InstrumentKeyTimeStamp(currentTime, key);
            usersKeyPressed.add(stamp);
            createDelayedRemoval(stamp);
        }
    }

    private MusicalEventListener getEventListener() {
        return new MusicalEventListener() {
            @Override
            public void onKeyPressed(InstrumentKey key) {
                handleMusicEvent(key);

                // propagate the event
                if (musicalEventListener != null) {
                    musicalEventListener.onKeyPressed(key);
                }
            }

            @Override
            public void onKeyReleased(InstrumentKey key) {

                key.release();

                InstrumentKeySequence sequence = getInstrumentKeySequence(key);
                sequence.keys.remove(key);

                if (!sequence.succededPlaying && sequence.keys.isEmpty()) {
                    missStreak();
                }

                if (sequence.keys.isEmpty()) {
                    synchronized (keyEvents) {
                        keyEvents.remove(sequence);
                    }
                }

                // propagate the event
                if (musicalEventListener != null) {
                    musicalEventListener.onKeyReleased(key);
                }
            }
        };
    }

    private void missStreak() {
        this.scoreMultiplier = MIN_SCORE_MULTIPLIER;
        this.currentStreakCount = 0;
        this.failedNoteCount++;
    }

    /**
     * Called when the user presses successfully the key in the maximum time
     * windows given.
     *
     * @param stamp The instrument key timestamp that succeded
     */
    private void handleSuccessPress(InstrumentKey key) {
        int pointsMade = POINTS_PER_SUCCES * scoreMultiplier;

        points += pointsMade;
        currentStreakCount++;
        successNoteCount++;
        if ((currentStreakCount != 0) && (currentStreakCount % NUMBER_OF_STREAK_FOR_MULTIPLIER_UP == 0) && (scoreMultiplier + 1 <= MAX_SCORE_MULTIPLIER)) {
            scoreMultiplier++;
        }

        inputHistoryEvent(key, pointsMade);
    }

    private void inputHistoryEvent(InstrumentKey key, int pointsMade) {

        synchronized (scoreHistory) {
            scoreHistory.addFirst(new ScoreEvent(key, pointsMade));

            if (scoreHistory.size() > POINT_HISTORY_MAX_SIZE) {
                scoreHistory.removeLast();
            }
        }
    }

    private void createDelayedRemoval(InstrumentKeyTimeStamp toRemove) {
        keyRemovingTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                // The player pressed a key that wasn't supposed to be played 
                synchronized (usersKeyPressed) {
                    if (usersKeyPressed.remove(toRemove)) {
                        missStreak();
                    }
                }
            }
        }, MAXIMUM_DELAY_BEFORE);
    }

    public void setMusicalEventListener(MusicalEventListener musicalEventListener) {
        this.musicalEventListener = musicalEventListener;
    }

    public double getSpeed() {
        return musicBox.getSpeedFraction();
    }

    public int getStreakCount() {
        return this.currentStreakCount;
    }

    private class InstrumentKeySequence {

        public Set<InstrumentKey> keys;
        public boolean succededPlaying;
        public long timestamp;

        public InstrumentKeySequence(long timestamp) {
            this.keys = new HashSet<>();
            succededPlaying = false;
            this.timestamp = timestamp;
        }
    }

    private class InstrumentKeyTimeStamp {

        public InstrumentKey key;
        public long timestamp;

        public InstrumentKeyTimeStamp(long timestamp, InstrumentKey key) {
            this.key = key;
            this.timestamp = timestamp;
        }
    }
}
