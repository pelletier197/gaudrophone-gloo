/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Note;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author sunny
 */
public class CompositionBuilder {
    
    private static final int DEFAULT_BPM = 60;
    
    private Map<Integer, Frame> frames;
    private int bpm;
    
    public CompositionBuilder() {
        this.frames = new TreeMap<>();
        this.bpm = DEFAULT_BPM;
    }

    /**
     * Sets the rhythm of the builder in BPM. This rhythm will be used to generate
     * the composition with the given tempo.
     *
     * @param bpm The beats per minute value
     */
    public void setRhythm(int bpm) {
        this.bpm = bpm;
    }

    /**
     * Adds a note and an octave to the frame with the given index.
     *
     * @param frame The frame index
     * @param type The type of note
     * @param n The note
     * @param octave The octave of the note
     */
    public void createNote(int frame, MusicalEventType type, Note n, int octave) {
        Frame f = getFrame(frame);
        f.addNote(type, n, octave);
        f.setPause(false);
    }

    /**
     * Sets the duration of the frame in measure of time, where 1 time is 1/bpm
     * second.
     *
     * @param frame The frame number to which is set the duration.
     * @param durationTime The duration of the frame in time, where 1 is 1 time.
     */
    public void setFrameDuration(int frame, double durationTime) {
        Frame f = getFrame(frame);
        f.durationTime = durationTime;
    }

    /**
     * Makes the frame with the given number a pause. This means, that no notes
     * will be played during the interval of time of the frame duration.
     *
     * @param frame The frame number of the pause.
     */
    public void makePause(int frame) {
        Frame f = getFrame(frame);
        if (f.musicalEvents.isEmpty()) {
            f.setPause(true);
        }
    }

    /**
     * Builds the composition using the rhythm, frame notes and pauses.
     *
     * @param name The name of the composition
     * @return The composition created from the frameEvents.
     */
    public Composition build(String name) {
        List<MusicalEvent> events = new ArrayList<> ();
        
        for (Frame frame : frames.values()) {
            
            if (frame.isPause()) {
                // Wait until next time
                long frameDuration = (long) (beatDuration() * frame.durationTime);
                events.add(new MusicalEvent(MusicalEventType.PAUSE, frameDuration));
            } else {
                // Play the keys
                for (MusicalEvent musicalEvent : frame.musicalEvents) {
                    events.add(musicalEvent);
                }
            }
        }
        return new Composition(events, name);
    }
    
    private long beatDuration() {
        return (long) (60.0 / bpm * 1000);
    }
    
    private Frame getFrame(int frame) {
        Frame f;
        
        if ((f = frames.get(frame)) == null) {
            f = new Frame();
            frames.put(frame, f);
        }
        
        return f;
    }
    
    private class Frame {
        
        private double durationTime;
        private boolean pause;
        private List<MusicalEvent> musicalEvents;
        
        public Frame() {
            this.durationTime = 1.0; // One time by default
            this.musicalEvents = new ArrayList<>();
        }
        
        public void addNote(MusicalEventType type, Note n, int octave) {
            if (type == MusicalEventType.PAUSE) {
                // Can't add a pause musical event as a note
                return;
            }
            FrameInfo frameInfo = new FrameInfo(n, octave);
            this.musicalEvents.add(new MusicalEvent(type, frameInfo));
        }
        
        public boolean isPause() {
            return pause;
        }
        
        public void setPause(boolean pause) {
            this.pause = pause;
        }
        
    }
    
}
