/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.InstrumentKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.IntStream;

/**
 *
 * @author Loup
 */
public class MusicBox {

    private Composition composition = null;
    private boolean playing = false;
    private boolean looping;
    private boolean demo;
    private MusicalEventListener listener;

    private List<List<InstrumentKey>> musicBoxKeys;
    private int currentReadingIndex;
    private Timer internalTimer;

    private double speedFraction = 1.0;

    public void setComposition(Composition composition) {
        // Can't set a composition if one is already playing.
        stop();

        if (composition != null && composition.numberOfFrames() == 0) {
            composition = null;
        }

        this.composition = composition;
        currentReadingIndex = 0;
    }

    public long getDuration() {
        return composition.getDuration();
    }

    public double getCompletionPercentage() {
        return (double) currentReadingIndex / composition.numberOfFrames();
    }

    public long getCurrentTime() {
        return (long) ((double) currentReadingIndex / composition.numberOfFrames() * composition.getDuration());
    }

    public void setMusicEventListener(MusicalEventListener eventListener) {
        listener = eventListener;
    }

    public boolean isPlaying() {
        return playing;
    }

    public MusicBox(Instrument instrument) {
        computeKeys(instrument);
        internalTimer = new Timer();
    }

    private void start(int startIndex, boolean looping, boolean demo, boolean doNotStartYet) {
        // Do nothing if no composition is loaded.
        if (composition == null) {
            return;
        }

        // Do nothing if already playing.
        if (playing) {
            return;
        }
        playing = true;

        if (startIndex > composition.numberOfFrames()) {
            startIndex = 0;
        }
        currentReadingIndex = startIndex;
        this.looping = looping;
        this.demo = demo;

        if (!doNotStartYet){
            playMusic();
        } else {
            playing = false;
        }
    }

    public void start() {
        start(0, false, false, false);
    }

    public void startDemo() {
        start(0, false, true, false);
    }

    public void startLoop() {
        start(0, true, false, false);
    }
    
    public void setAt(double fraction) {
        if (fraction < 0 || fraction > 1) {
            throw new IllegalArgumentException("A fraction is between 0 and 1");
        }

        long playedTime = (long) (fraction * composition.duration);
        int startIndex = composition.seekFrame(playedTime);
        
        start(startIndex, false, false, true);
    }

    public void startAt(double fraction) {
        if (fraction < 0 || fraction > 1) {
            throw new IllegalArgumentException("A fraction is between 0 and 1");
        }

        long playedTime = (long) (fraction * composition.duration);
        int startIndex = composition.seekFrame(playedTime);

        start(startIndex, false, false, false);
    }

    public void resume() {
        if (currentReadingIndex >= composition.numberOfFrames()) {
            currentReadingIndex = 0;
        }
        start(currentReadingIndex, looping, demo, false);
    }

    public void stop() {
        internalTimer.cancel();
        internalTimer = new Timer();
        playing = false;
        musicBoxKeys.forEach(m -> m.forEach(i -> i.release()));
    }

    private void playMusic() {
        while (playing) {

            MusicalEvent currentEvent = composition.eventAtFrame(currentReadingIndex);
            currentReadingIndex += 1;

            if (looping) {
                currentReadingIndex = currentReadingIndex % composition.numberOfFrames();
            } else if (currentReadingIndex >= composition.numberOfFrames()) {
                playing = false;
            }

            Object eventData = currentEvent.data;
            switch (currentEvent.type) {
                case KEY_PRESS:
                    pressKey((FrameInfo) eventData);

                    break;
                case KEY_RELEASE:
                    releaseKey((FrameInfo) eventData);

                    break;
                case PAUSE:
                    internalTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            playMusic();
                        }
                    }, (long) (((long) eventData) / speedFraction));
                    return;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    private void pressKey(FrameInfo eventData) {
        int noteIndex = eventData.getMidiNote();
        List<InstrumentKey> keys = musicBoxKeys.get(noteIndex);
        for (InstrumentKey key : keys) {
            if (demo) {
                key.pressDemo();
            } else {
                key.press();
            }
            if (listener != null) {
                listener.onKeyPressed(key);
            }
        }
    }

    private void releaseKey(FrameInfo eventData) {
        int noteIndex = eventData.getMidiNote();
        List<InstrumentKey> keys = musicBoxKeys.get(noteIndex);
        for (InstrumentKey key : keys) {
            if (demo) {
                key.releaseDemo();
            } else {
                key.release();
            }
            if (listener != null) {
                listener.onKeyReleased(key);
            }
        }
    }

    private void computeKeys(Instrument instrument) {
        // TODO use constant here
        musicBoxKeys = new ArrayList<>(128);
        IntStream.range(0, 128).forEach((i) -> {
            musicBoxKeys.add(new ArrayList<>());
        });

        List<InstrumentKey> instrumentKeys = instrument.getInstrumentKeys();
        instrumentKeys.forEach((instrumentKey) -> {
            if (instrumentKey.isPlayable()) {
                int noteIndex = instrumentKey.getSound().getMidiNote();
                musicBoxKeys.get(noteIndex).add(instrumentKey);
            }
        });
    }

    public Composition getComposition() {
        return composition;
    }

    public void setSpeed(double fraction) {
        if (fraction > 0) {
            this.speedFraction = fraction;
        }
    }

    public double getSpeedFraction() {
        return this.speedFraction;
    }

    public void setDemo(boolean b) {
        this.demo = b;
    }
}
