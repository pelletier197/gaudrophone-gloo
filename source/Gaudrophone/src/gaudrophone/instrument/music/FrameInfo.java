/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Note;
import gaudrophone.instrument.sound.Sound;
import java.io.Serializable;

/**
 *
 * @author sunny
 */
public class FrameInfo implements Serializable {

    private final Note note;
    private final int octave;
        
    public Note getNote() {
        return note;
    }
    
    public int getOctave() {
        return octave;
    }
    
    public int getMidiNote() {
        return Sound.computeMidiNoteFromNoteOctave(note, octave);
    }

    public FrameInfo(Note note, int octave) {
        this.note = note;
        this.octave = octave;
    }
}
