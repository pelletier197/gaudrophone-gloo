/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.InstrumentKey;

/**
 *
 * @author sunny
 */
public interface MusicalEventListener {
    
    public void onKeyPressed(InstrumentKey key);
    
    public void onKeyReleased(InstrumentKey key);
}
