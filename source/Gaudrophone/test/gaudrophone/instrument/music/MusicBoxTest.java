/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gaudrophone.instrument.music;

import gaudrophone.instrument.Instrument;
import gaudrophone.instrument.Note;
import gaudrophone.instrument.template.PianoTemplateStrategy;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loup
 */
public class MusicBoxTest {
    
    protected static Composition composition1;
    protected static Composition composition2;
    protected static Instrument instrument1;
    protected MusicBox musicBox1;
    
    public MusicBoxTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        List<MusicalEvent> eventList = new ArrayList<>();
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.D, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.D, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.D_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.D_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.E, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.E, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.F, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.F, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.F_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.F_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.G, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.G, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.G_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.G_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.A, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.A, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.A_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.A_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.B, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.B, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C, 5)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C, 5)));
        composition1 = new Composition(eventList, "");
        eventList.clear();
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 5000L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_PRESS, new FrameInfo(Note.C_SHARP, 4)));
        eventList.add(new MusicalEvent(MusicalEventType.PAUSE, 500L));
        eventList.add(new MusicalEvent(MusicalEventType.KEY_RELEASE, new FrameInfo(Note.C_SHARP, 4)));
        composition2 = new Composition(eventList, "");
        instrument1 = new PianoTemplateStrategy().generateInstrument();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        musicBox1 = new MusicBox(instrument1);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setComposition method, of class MusicBox.
     */
    @Test
    public void testSetComposition() {
        System.out.println("setComposition");
        Composition composition = composition1;
        musicBox1.setComposition(composition);
    }

    /**
     * Test of isPlaying method, of class MusicBox.
     */
    @Test
    public void testIsPlaying() {
        System.out.println("isPlaying");
        boolean expResult = false;
        boolean result = musicBox1.isPlaying();
        assertEquals(expResult, result);
    }

    /**
     * Test of start method, of class MusicBox.
     */
    @Test
    public void testStart() {
        System.out.println("Start");
        musicBox1.setComposition(composition1);
        musicBox1.start();
        listen();
    }

    /**
     * Test of startLoop method, of class MusicBox.
     */
    @Test
    public void testStartLoop() {
        System.out.println("Start");
        musicBox1.setComposition(composition1);
        musicBox1.startLoop();
        listen();
    }

    /**
     * Test of StartAt method, of class MusicBox.
     * @throws java.lang.InterruptedException
     */
    @Test
    public void testStartAt() throws InterruptedException {
        System.out.println("StartAt");
        double fraction = 0.50;
        musicBox1.setComposition(composition1);
        musicBox1.startAt(fraction);
        listen();
    }

    /**
     * Test of StartAt_longPause method, of class MusicBox.
     * @throws java.lang.InterruptedException
     */
    @Test
    public void testStartAt_longPause() throws InterruptedException {
        System.out.println("StartAt");
        double fraction = 0.50;
        musicBox1.setComposition(composition2);
        musicBox1.startAt(fraction);
        listen();
    }

    /**
     * Test of Resume method, of class MusicBox.
     */
    @Test
    public void testResume() {
        System.out.println("Resume");
        musicBox1.setComposition(composition1);
        musicBox1.start();
        listen();
        musicBox1.stop();
        listen();
        musicBox1.resume();
        listen();
    }

    /**
     * Test of Stop method, of class MusicBox.
     */
    @Test
    public void testStop() {
        System.out.println("Stop");
        musicBox1.stop();
    }
    
    private void listen() {
    }
    
}
