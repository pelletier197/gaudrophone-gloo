/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author sunny
 */
public class TitledPanel extends JPanel {

    private static final int DEFAULT_FONT = 18;

    private TitledBorder border;
    private int fontSize;
    private String borderName;

    public TitledPanel(){
        this("");
    }
    
    public TitledPanel(String borderName) {
        this(borderName, DEFAULT_FONT);
    }

    public TitledPanel(String borderName, int fontSize) {
        super();
        this.fontSize = fontSize;
        this.borderName = borderName;
        init();
    }

    public String getBorderName() {
        return borderName;
    }

    public void setBorderName(String borderName) {
        this.border.setTitle(borderName);
        this.borderName = borderName;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        Font current = border.getTitleFont();
        border.setTitleFont(new Font(current.getFamily(), Font.BOLD, fontSize));
        this.fontSize = fontSize;
    }

    private void init() {
        border = new TitledBorder(borderName);
        border.setTitlePosition(TitledBorder.CENTER);
        setFontSize(fontSize);
        setBorder(border);
    }

}
