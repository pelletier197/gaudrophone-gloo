// Deku Palace (Koji Kondo)
112

| E3 B2 B2 B2 B2 E3 B2 B2 X | E3 B2 B2 B2 E3 E3 B2 B2 B2 X |
| ,  .  .  ,  ,  ,  ,  ,  , | ,  .  .  ,  ,  ,  .  .  ,  , |


| E4 B4 C5 B4 A4 G4 A4 G4 F4# E4 E4 | E4 G4 A4 G4 F4# E4 F4# E4  D4 E4    |
| ,  .  .  ,  ,  .  .  .  .   ,  ,  | ,  .  .  ,  ,   .  .   .   .  _     |
| B3 X     B3 X  B3    X      B3 X  | B3 X     B3 X   B3     A3     B3 X  |
| G3 X     G3 X  G3    X      G3 X  | G3 X     G3 X   G3     F3#    G3 X  |
| E3 X     E3 X  E3    X      E3 X  | E3 X     E3 X   E3     D3     E3 X  |
| ,  ,     ,  ,  ,     ,      ,  ,  | ,  ,     ,  ,   ,      ,      ,  ,  |


| E4 B4 C5 B4 A4 G4 A4 G4 F4# E4 E4 | E4 F4# G4 A4 G4 F4# E4 D4  E4    | E5 B5 C6 B5 A5 G5 A5 G5 F5# E5 E5 | E5 G5 A5 G5 F5# E5 F5# E5 D5 E5    |
| ,  .  .  ,  ,  .  .  .  .   ,  ,  | .  .   .  .  ,  ,   ,  ,   _     | ,  .  .  ,  ,  .  .  .  .   ,  ,  | ,  .  .  ,  ,   .  .   .  .  _     |
| B3 X     B3 X  B3    X      B3 X  | B3     X     B3 X   B3 A3  B3 X  | E4 X     E4 X  E4    X      E4 X  | E4 X     E4 X   E4     D4    E4 X  |
| G3 X     G3 X  G3    X      G3 X  | G3     X     G3 X   G3 F3# G3 X  | B3 X     B3 X  B3    X      B3 X  | B3 X     B3 X   B3     A3    B3 X  |
| E3 X     E3 X  E3    X      E3 X  | E3     X     E3 X   E3 D3  E3 X  | G3 X     G3 X  G3    X      G3 X  | G3 X     G3 X   G3     F3#   G3 X  |
| ,  ,     ,  ,  ,     ,      ,  ,  | ,      ,     ,  ,   ,  ,   ,  ,  | ,  ,     ,  ,  ,     ,      ,  ,  | ,  ,     ,  ,   ,      ,     ,  ,  |


| E5 B5 C6 B5 A5 G5 A5 G5 F5# E5 E5 | G5 A5 G5 F5# E5 D5 E5 E5  E5    | D5  A4 A4  B4 C5 D5 C5 B4 B4 G4 | D5  A4 A4  B4 C5 D5 B4          |
| ,  .  .  ,  ,  .  .  .  .   ,  ,  | ,  .  .  ,   .  .  ,  ,   _     | ,   ,  .   .  .  .  ,  ,  ,  ,  | ,   ,  .   .  .  .  2           |
| E4 X     E4 X  E4    X      E4 X  | E4 X     E4  X     E4 D4  E4 X  | D4  X  D4     X     B3 B3 B3 X  | D4  X  D4     X     B3 B3 B3 X  |
| B3 X     B3 X  B3    X      B3 X  | B3 X     B3  X     B3 A3  B3 X  | A3  X  A3     X     G3 G3 G3 X  | A3  X  A3     X     G3 G3 G3 X  |
| G3 X     G3 X  G3    X      G3 X  | G3 X     G3  X     G3 F3# G3 X  | F3# X  F3#    X     D3 D3 D3 X  | F3# X  F3#    X     D3 D3 D3 X  |
| ,  ,     ,  ,  ,     ,      ,  ,  | ,  ,     ,   ,     ,  ,   ,  ,  | ,   ,  ,      ,     ,  ,  ,  ,  | ,   ,  ,      ,     ,  ,  ,  ,  |


| D5  A4 A4  B4 C5 D5 B4 G4 G4 B4 | A4  G4 F4# E4  F4# D4# E4          | D5  A4 A4  B4 C5 D5 C5 B4 B4 G4 | D5  A4 A4  B4 C5 D5 B4          |
| ,   ,  .   .  .  .  ,  ,  ,  ,  | ,   .  .   ,   .   .   2           | ,   ,  .   .  .  .  ,  ,  ,  ,  | ,   ,  .   .  .  .  2           |
| D4  X  D4     X     B3 B3 B3 X  | D4# X      D4# X       E4 E4 E4 X  | D4  X  D4     X     B3 B3 B3 X  | D4  X  D4     X     B3 B3 B3 X  |
| A3  X  A3     X     G3 G3 G3 X  | B3  X      B3  X       B3 B3 B3 X  | A3  X  A3     X     G3 G3 G3 X  | A3  X  A3     X     G3 G3 G3 X  |
| F3# X  F3#    X     D3 D3 D3 X  | F3# X      F3# X       G3 G3 G3 X  | F3# X  F3#    X     D3 D3 D3 X  | F3# X  F3#    X     D3 D3 D3 X  |
| ,   ,  ,      ,     ,  ,  ,  ,  | ,   ,      ,   ,       ,  ,  ,  ,  | ,   ,  ,      ,     ,  ,  ,  ,  | ,   ,  ,      ,     ,  ,  ,  ,  |


| D5  A4 A4  B4 C5 D5 B4 G4 G4 B4 | A4  G4 F4# E4  F4# D4# E4          | E4 B4 C5 B4 A4 G4 A4 G4 F4# E4 E4 | E4 G4 A4 G4 F4# E4 F4# E4  D4 E4    |
| ,   ,  .   .  .  .  ,  ,  ,  ,  | ,   .  .   ,   .   .   2           | ,  .  .  ,  ,  .  .  .  .   ,  ,  | ,  .  .  ,  ,   .  .   .   .  _     |
| D4  X  D4     X     B3 B3 B3 X  | D4# X      D4# X       E4 E4 E4 X  | B3 X     B3 X  B3    X      B3 X  | B3 X     B3 X   B3     A3     B3 X  |
| A3  X  A3     X     G3 G3 G3 X  | B3  X      B3  X       B3 B3 B3 X  | G3 X     G3 X  G3    X      G3 X  | G3 X     G3 X   G3     F3#    G3 X  |
| F3# X  F3#    X     D3 D3 D3 X  | F3# X      F3# X       G3 G3 G3 X  | E3 X     E3 X  E3    X      E3 X  | E3 X     E3 X   E3     D3     E3 X  |
| ,   ,  ,      ,     ,  ,  ,  ,  | ,   ,      ,   ,       ,  ,  ,  ,  | ,  ,     ,  ,  ,     ,      ,  ,  | ,  ,     ,  ,   ,      ,      ,  ,  |


| E4 B4 C5 B4 A4 G4 A4 G4 F4# E4 E4 | E4 F4# G4 A4 G4 F4# E4 D4  E4    |
| ,  .  .  ,  ,  .  .  .  .   ,  ,  | .  .   .  .  ,  ,   ,  ,   _     |
| B3 X     B3 X  B3    X      B3 X  | B3     X     B3 X   B3 A3  B3 X  |
| G3 X     G3 X  G3    X      G3 X  | G3     X     G3 X   G3 F3# G3 X  |
| E3 X     E3 X  E3    X      E3 X  | E3     X     E3 X   E3 D3  E3 X  |
| ,  ,     ,  ,  ,     ,      ,  ,  | ,      ,     ,  ,   ,  ,   ,  ,  |


| B4             B4             | B4       A4 C5 B4             |
| E4             E4             | E4       E4 E4 E4             |
| 2              2              | _        ,  ,  2              |
| E3 B2 B2 B2 B2 E3 B2 B2 B2 X  | E3 B2 B2 B2 E3 E3 B2 B2 B2 X  |
| ,  .  .  ,  ,  ,  .  .  ,  ,  | ,  .  .  ,  ,  ,  .  .  ,  ,  |


| B4             B4             | A4       G4 F4# E4             | B4             B4             | B4       A4 C5 B4             |
| E4             E4             | D4       D4 D4  B3             | E4             E4             | E4       E4 E4 E4             |
| 2              2              | _        ,  ,   2              | 2              2              | _        ,  ,  2              |
| E3 B2 B2 B2 B2 E3 B2 B2 B2 X  | E3 B2 B2 B2 E3  E3 B2 B2 B2 X  | E3 B2 B2 B2 B2 E3 B2 B2 B2 X  | E3 B2 B2 B2 E3 E3 B2 B2 B2 X  |
| ,  .  .  ,  ,  ,  .  .  ,  ,  | ,  .  .  ,  ,   ,  .  .  ,  ,  | ,  .  .  ,  ,  ,  .  .  ,  ,  | ,  .  .  ,  ,  ,  .  .  ,  ,  |


| B4             B4             | A4       G4 F4# E4             |
| E4             E4             | D4       D4 D4  B3             |
| 2              2              | _        ,  ,   2              |
| E3 B2 B2 B2 B2 E3 B2 B2 B2 X  | E3 B2 B2 B2 E3  E3 B2 B2 B2 X  |
| ,  .  .  ,  ,  ,  .  .  ,  ,  | ,  .  .  ,  ,   ,  .  .  ,  ,  |
