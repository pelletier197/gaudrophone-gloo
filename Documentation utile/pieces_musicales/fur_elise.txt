// Fur elise de Beethoven
70

// Ligne 1
| E5 D5# E5 D5# E5 B4 D5 C5 A4 X  X  C4  E4  A4  B4  X   X   E4 G4#  B4  C5  X   X  E4 |
|  X  X   X   X   X  X  X  X A2 E3 A3 X   X   X   E2  E3 G3#  X  X    X   A2  E3  A3 X |
|  .  .   .  .   .  .  .   .  . .  .  .   .   .   .    .  .   .   .   .   .  .    .  . |

// Ligne 2
| E5 D5# E5 D5# E5 B4 D5 C5 A4 X  X  C4 E4 A4  B4  X   X    E4  C5  B4  A4  X  X  |
| X  X   X   X   X  X  X  X A2 E3 A3 X  X  X   E2  E3  G3#  X   X   X   A2  E3 A3 |
| .  .   .  .   .  .  .   .  . .  .  .  .  .   .   .   .    .   .   .   .    .  . |
